import java.util.*;  
​
​
class Main {
​
  static ArrayList<String> pairsList = new ArrayList<String>();
  static Scanner s = new Scanner(System.in); 
​
  static int[] readArray(){
    
    System.out.println("how many integers do you want in the array?");
    int arrayLength = s.nextInt();
    System.out.println("you gone write : " + arrayLength + " integers");
    int[] array = new int[arrayLength];
    for(int i = 0; i < arrayLength; i++){
      array[i] = s.nextInt();
    } 
​
    return array;
  }
​
  static int readSum(){
    System.out.println("write the sum number?");
    return s.nextInt();
  }
​
  static void findNumberOfPairs(int[] array , int sum){
    if(array.length == 1){
      System.out.println("================");
      return;
    }
​
    int firstPairNumber = array[0];
    int[] sliceOfArray = Arrays.copyOfRange(array, 1, array.length);
    findSecondPairsInRestOfArray(firstPairNumber,sum,sliceOfArray);
    findNumberOfPairs(sliceOfArray,sum);
  }
​
  static void findSecondPairsInRestOfArray(int firstPairNumber,int sum ,int[]sliceOfArray){
​
    for(int i = 0; i < sliceOfArray.length; i++){  
        if((firstPairNumber + sliceOfArray[i]) == sum){
           pairsList.add( " (" + firstPairNumber + "," + sliceOfArray[i] +")");
        }
    }
  }
​
  public static void main(String[] args) {
    //System.out.println("Hello world!");
   int[] numbersArray = readArray();
   int sumNumber = readSum();
   System.out.println(
     "Input : numbers[] =" + Arrays.toString(numbersArray) + "  somme = " + sumNumber
    );
   findNumberOfPairs(numbersArray,sumNumber);
   System.out.println("Output  : " + pairsList);
  }
}
